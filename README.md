# Moubootaur Legends Website


### Usage

```sh
# first run (install dependencies)
npm i
# build
npm run build
```

### File structure
```sh
├── assets                  # static files such as images
├── generator               # source code of the page generator
├── i18n                    # translation files
│   ├── de.yml
│   └── en.yml
├── node_modules            # third party modules
├── package.json
├── package-lock.json
├── pages                   # all files in this folder and nested folders get rendered
│   └── templates           # .. except for the templates in the template folder (stuff that gets reused by many files) 
├── README.md
└── styles                  # the style sources are here
    └── site.scss
```
### Translations

You can translate on [Transifex](https://www.transifex.com/arctic-games/moubootaur-legends/website).

Our [Tool](https://gitlab.com/TMW2/evol-tools/blob/master/web/updatelang.py) will convert the pot files to valid YAML.

This page has a very basic key-value translation system.

Language files are stored as `yaml` under the `i18n` folder.

When a string for a key is not found in your language, its taken from the english lang and when it's not even found there the key gets printed as it and you get a warning when building the page.

You can use `$s` to define places for substitutions.

###### Example
de.yml
```yaml
test_key: eine Zeihenkette um testen
```
```js
const {tx} = getTranslateFunction('de')
tx('test_key') // "eine Zeihenkette um testen"
const tx_en = getTranslateFunction('en')
tx_en('test_key') // "a string for testing"
```
###### Example with a substitution:
en.yml
```yaml
player_online_count: $s Online Players
```
```js
const {tx} = getTranslateFunction('en')
tx('player_online_count', ['5']) // "5 Online Players"
```

##### Translation with a Link
en.yml
```yaml
test_link: String with a [link]
```
```js
const {txLink} = getTranslateFunction('de')
txLink('test_link', 'https://tmw2.org') // "String with a <a href="tmw2.org">link</a>"
```

## Notes
this section needs to be integrated into the readme, at the moment its just loose notes.


pages: every file in this folder gets rendered beside the ones in the template folder
in the template foder there will be a `main.ejs` which .


transforms into: (en is standard lang)
- _site
 - assets
 - en
  .. sites
 - de
  .. sites
- fr
  .. sites
- br
  .. sites

- feed.xml

readme: 

server config: 
 rewrite direct requests to languge page
 (tmw2.org to tmw2.org/[langcode] and tmw2.org/news to tmw2.org/[langcode]/news (even though the actual news are not translated))

 but feed.xml shouldn't be rewritten
 also static folder shouldn't be rewritten, its shared between all languages

 (nearest 404.html via try files, add example in readme)

- let https://tmw2.org/online.json through
- let https://tmw2.org/feed.xml through
- expose experiments directory


## TODO:

- [X] Switch Languages (current page in different lang, added to footer or somewhere else inside site.ejs )
- [ ] Localize Remaining Pages
 - [ ] Home Page
 - [ ] Get Involved sub-pages (we might wait a bit with the content on those ones, because there isn't enough to translate there yet)
- [-] News -> postponed, for now we'll still use the old system more about that in the `markdown_news` branch
- [ ] Error Pages (localized) (really only 404)
- [ ] finish readme
    - [ ] info about output folder structure
    - [ ] info about server config
    - [ ] introduction sentence
- [ ] release first version

- [ ] (nice to have) Tests
 - [ ] validize input while building
 - [ ] tests for translate module

### Website adjustments:

- serve icons localy - use font awesome as npm package
