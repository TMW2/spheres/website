const RSS_FEED = "https://spheres.tmw2.org/feed.xml";
const HIDE_THIS = [
  "If you ever run in trouble, try contacting a GM with <b>@request Help me!</b>",
  "<br> Our Staff will never ask for your password. You are the sole responsible for<br> its safety!<br> ",
  "By playing you agree and abide to the <b>Terms of Service</b>, available at:<br> <a href=\"https://tmw2.org/legal.php\">https://tmw2.org/legal.php</a><br>",
  " Please enjoy our server, and have fun!<br>",
  "You can contact a GM with <b>@request</b> or calling one at #world.<br>",
  " (If you want, it's possible to skip music download under Settings > Audio)",
  "<br> Enjoy gaming, and leave feedback!",
  "<br> You can check out this page for older entries:<br> <a href=\"https://tmw2.org/news\">https://tmw2.org/news</a><br>",
  "<br> You can check out this page for older entries:<br> <a href=\"https://tmw2.org/news.html\">https://tmw2.org/news</a><br>",
  "<br> You can check out this page for older entries:<br> <a href=\"http://tmw2.org/news.html\">http://tmw2.org/news</a><br>"
];
const CARD_TEMPLATE = '\
<div class="news_card">\
   <div class="news_title" id="[[DateID]]" name="[[DateID]]">\
     <a href="#[[DateID]]"><i class="fas fa-link"></i></a>\
      [[Title]] <smal>([[DateID]])<smal>\
   </div>\
   <div class="news_line"></div>\
   [[Content]]\
   <br>\
</div>';
const TOC_ENTRY_TEMPLATE = '<a href="#[[DateID]]"><i class="fas fa-chevron-circle-right"></i> <span>[[DateID]]</span><br></a>';

function getNews(){
  return new Promise((res, rej) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       res(this.responseText);
     } else if (this.readyState == 4) {
       rej({msg:"Loading news failed",state:this.status,data:this.responseText});
     }
    };
    xhttp.open("GET", RSS_FEED, true);
    xhttp.send();
});
}

function parse_news(){
  return new Promise((res, rej) => {
    getNews().then((rawdata) => {
      var result={};
      parser = new DOMParser();
      feed = parser.parseFromString(rawdata,"text/xml");
      result.title = feed.getElementsByTagName("title")[0].innerHTML;
      result.updated = feed.getElementsByTagName("updated")[0].innerHTML;
      var author = feed.getElementsByTagName("author")[0];
      result.author = {
        name: author.getElementsByTagName("name")[0].innerHTML,
        email:author.getElementsByTagName("email")[0].innerHTML
      };
      result.entries = [];
      var entries = feed.getElementsByTagName("entry");
      for (var i = 0; i < entries.length; i++) {
        var entry = entries[i];
        var content = entry.getElementsByTagName("content")[0].firstChild.data;
        // Reformat content
        content = content.replace(/(\r\n|\r|\n)/g, "").replace(/<p>/g, "").replace(/<\/p>/g, "<br>");
        result.entries.push({
          title: entry.getElementsByTagName("title")[0].innerHTML,
          id: entry.getElementsByTagName("link")[0].attributes.href.nodeValue.split('#')[1],
          updated: entry.getElementsByTagName("updated")[0].innerHTML,
          content: clearContent(content)
        });
      }
      res(result);
    }).catch(rej);
  });
}
function clearContent(content){
  // Hides things that are not News like the advises that are only usefull when the news are displayed on the server
  var newContent = content;
  for (var i = 0; i < HIDE_THIS.length; i++) {
    newContent = newContent.replace(HIDE_THIS[i],"");
  }
  return newContent;
}
function generate(entry, template){
  var card = template.toString();
  card = card.replace(/\[\[DateID\]\]/g, entry.id)
  .replace(/\[\[Title\]\]/g, entry.title)
  .replace(/\[\[Content\]\]/g, entry.content);
  return card;
}
function displayIt(data, card_container, toc_container){
  for (var i = 0; i < data.entries.length; i++) {
    card_container.insertAdjacentHTML("beforeend", generate(data.entries[i],CARD_TEMPLATE));
    toc_container.insertAdjacentHTML("beforeend", generate(data.entries[i],TOC_ENTRY_TEMPLATE));
  }
  // IDEA: Should we also display some of the feeds meta data somewhere else?
}
