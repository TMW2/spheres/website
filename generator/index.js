const fs = require('fs-extra')
const { join } = require('path')
const { promisify } = require('util')
const sass = require('node-sass');

const { load, getTranslateFunction } = require('./i18n')
const { pages } = require('./pages')

async function main() {
   const baseDir = join(__dirname, '..')

   await load(join(baseDir, './i18n'))

   let { tx } = getTranslateFunction('de')
   console.log(tx('test_key'), tx('player_online_count', ['5']))

   const output_dir = join(baseDir, './output')
   await fs.ensureDir(output_dir)
   await fs.emptyDir(output_dir)

   await pages(join(baseDir, './pages'), output_dir)

   // static

   await fs.copy(join(baseDir, 'assets'), join(output_dir, 'static'))

   // styles - scss

   const ScssResult = await promisify(sass.render)({
      file: join(__dirname, '../styles/main.scss'),
      outputStyle: 'compressed'
   })

   await fs.copyFile(
      join(__dirname, '../node_modules/normalize.css/normalize.css'),
      join(output_dir, 'static', 'normalize.css')
   )

   await fs.copyFile(
      join(__dirname, '../node_modules/@fortawesome/fontawesome-free/js/all.min.js'),
      join(output_dir, 'static', 'fontawesome.js')
   )

   await fs.writeFile(
      join(output_dir, 'static', 'site.css'),
      ScssResult.css
   )
}

main();