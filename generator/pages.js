const fs = require('fs-extra')
const { join, dirname } = require('path')
const walker = require('folder-walker')
const { renderFile } = require('ejs');

const { getLangs, getTranslateFunction } = require('./i18n')

async function pages(pages_dir, output_dir) {

    const pages = await readdir(pages_dir)

    const langs = Object.keys(getLangs())

    const renderPage = async (file, data) => {
        return await renderFile(join(pages_dir, 'templates/site.ejs'), {
            ...data,
            content: await renderFile(file, data, {})
        }, {})
    }

    for (let ii = 0; ii < langs.length; ii++) {
        const lang_code = langs[ii];
        const directory = await mkDirJoin(output_dir, lang_code)
        const { tx, txLink } = getTranslateFunction(lang_code)

        for (let j = 0; j < pages.length; j++) {
            const name = pages[j];

            const sourcefile = join(pages_dir, name)
            const destfile = join(directory, name).replace(/ejs$/, 'html')

            if (name.indexOf("/") !== -1) {
                await fs.ensureDir(dirname(destfile))
            }

            const content = await renderPage(sourcefile, {
                page_name: name,
                tx,
                txLink,
                lang_code,
                languages: getLangs()
            })

            await fs.writeFile(destfile, content)
        }
    }

    // TODO news

}

async function mkDirJoin(...dirs) {
    const dir = join(...dirs)
    await fs.ensureDir(dir)
    return dir
}

/**
 * 
 * @returns {Promise<string[]>}
 */
function readdir(dir) {
    return new Promise((resolve, reject) => {
        let pages = []
        var stream = walker([dir])
        stream.on('data', function ({ basedir, type, relname }) {
            if (relname.includes('templates/') || type == 'directory')
                return;
            pages.push(relname)
        })
        stream.on('end', _ => resolve(pages))
    })
}


module.exports = {
    pages
}