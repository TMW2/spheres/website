const fs = require('fs-extra')
const { join, basename } = require('path')
const yaml = require('yaml')


let languages = {};
let languages_meta = {};
let defaultLang = {};

async function load(langDir) {
    const dir = await fs.readdir(langDir)

    languages = {}
    languages_meta = {}
    for (let i = 0; i < dir.length; i++) {
        const filename = dir[i];
        const data = await parseLangFile(join(langDir, filename))

        languages[basename(filename, '.yml')] = data
        languages_meta[basename(filename, '.yml')] = data.language_meta
    }

    defaultLang = languages['en']
}

function getLangs() {
    return languages_meta
}

function getTranslateFunction(lang_name) {
    const lang = languages[lang_name]
    if (!lang) throw new Error(`[i18n] Language '${lang_name}' not found`)

    return {
        tx: tx.bind(null, lang),
        txLink: txLink.bind(null, lang)
    }
}

/**
 * 
 * @param {string} key 
 * @param {string[]} substitutions 
 */
function tx(lang, key, substitutions) {
    const value = lang[key] || defaultLang[key]
    if (!value) {
        console.warn(`[i18n] '${key}' not found`)
        return `i18n.${key}`
    }
    let i = 0;
    if (substitutions) {
        return value.replace(/\$s/g, () => {
            const replacement = substitutions[i++]
            if (typeof (replacement) === "undefined")
                return '$s'
            else
                return replacement.toString()
        })
    } else {
        return value
    }
}

/**
 * @param {string} key
 * @param {string} link
 */
function txLink(lang, key, link) {
    const value = lang[key] || defaultLang[key]
    if (!value) {
        console.warn(`[i18n] '${key}' not found`)
        return `i18n.${key}`
    }
    if (link) {
        return value
         .replace(/(\[([^<>\]\[]+)\])/, `<a href="${link}">$2</a>`)
    } else {
        return value
    }
}

async function parseLangFile(path) {
    const textdata = await fs.readFile(path, 'utf-8')
    // TODO test for language meta data completness
    return yaml.parse(textdata)
}



module.exports = {
    load,
    getLangs,
    getTranslateFunction
}