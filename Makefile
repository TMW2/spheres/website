all:
	npm i
	npm run build

deploy:
	rm -rf output/*
	rm -rf */*~
	npm i
	npm run build
	cp output/en/404.html output/404.html
	cp languages.html output/index.html
	cp favicon.ico output/
	find output/

